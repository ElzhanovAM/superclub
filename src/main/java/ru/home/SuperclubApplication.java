package ru.home;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import ru.home.security.JwtAuthenticationRequest;
import ru.home.service.AccountService;

@SpringBootApplication
public class SuperclubApplication {
	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(SuperclubApplication.class, args);
		AccountService accountService = applicationContext.getBean(AccountService.class);
		accountService.saveAccount(new JwtAuthenticationRequest("admin", "password"));
	}
}
