package ru.home.validator;

import org.apache.commons.validator.routines.UrlValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class VkProfileValidBeanConstraintValidator implements ConstraintValidator<VkProfileValueValidBeanConstraint, String> {

    @Override
    public boolean isValid(String referenceValue, ConstraintValidatorContext constraintValidatorContext) {
        if (referenceValue == null) return true;
        String[] schemes = {"http","https"};
        UrlValidator urlValidator = new UrlValidator(schemes);
        return urlValidator.isValid(referenceValue);
    }

    @Override
    public void initialize(VkProfileValueValidBeanConstraint constraintAnnotation) {

    }
}