package ru.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.home.entity.DemoVideo;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface DemoVideoRepository extends JpaRepository<DemoVideo, Integer> {

    @Modifying
    @Query("update DemoVideo dV set dV.likes = dV.likes + 1 where id = :id")
    void like(@Param("id")int id);

    @Modifying
    @Query("update DemoVideo dV set dV.likes = dV.likes - 1 where id = :id")
    void dislike(@Param("id")int id);
}