package ru.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.home.entity.Event;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface EventRepository extends JpaRepository<Event, Integer> {
    @Modifying
    @Query("update Event e set e.go = e.go + 1 where id = :id")
    void go( @Param("id") int id);

    @Modifying
    @Query("update Event e set e.mayBe = e.mayBe + 1 where id = :id")
    void maybe( @Param("id") int id);
}