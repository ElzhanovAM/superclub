package ru.home.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.home.entity.Club;

@Repository
public interface ClubRepository extends JpaRepository<Club, Integer> {
}
