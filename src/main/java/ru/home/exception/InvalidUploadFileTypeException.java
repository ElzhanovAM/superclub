package ru.home.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidUploadFileTypeException extends RuntimeException {
  public InvalidUploadFileTypeException() {
  }

  public InvalidUploadFileTypeException(String message) {
    super(message);
  }

  public InvalidUploadFileTypeException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidUploadFileTypeException(Throwable cause) {
    super(cause);
  }

  public InvalidUploadFileTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
