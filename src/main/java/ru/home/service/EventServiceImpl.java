package ru.home.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.home.entity.Club;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.exception.DuplicateException;
import ru.home.filter.EventFilter;
import ru.home.repository.EventRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {
    private EventRepository eventRepository;
    private InstructorService instructorService;
    private ClubService clubService;

    public EventServiceImpl(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }


    @Autowired
    public void setInstructorService(InstructorService instructorService) {
        this.instructorService = instructorService;
    }

    @Autowired
    public void setClubService(ClubService clubService) {
        this.clubService = clubService;
    }

    @Override
    public List<Event> findEvents(EventFilter filter) {
        List<Event> events = eventRepository.findAll();
        if (!StringUtils.isEmpty(filter.getCity())) {
            events = events
                    .stream()
                    .filter(a -> a.getClub().getCity().equals(filter.getCity()))
                    .collect(Collectors.toList());
        }

        if (!StringUtils.isEmpty(filter.getInstructor())) {
            events = events
                    .stream()
                    .filter(a -> a.getOrganizer().getFullName().equals(filter.getInstructor()))
                    .collect(Collectors.toList());
        }
        if (filter.getDateFrom() != null) {
            events = events
                    .stream()
                    .filter(a -> a.getDate().after(filter.getDateFrom()))
                    .collect(Collectors.toList());
        }
        if (filter.getDateTo() != null) {
            events = events
                    .stream()
                    .filter(a -> a.getDate().before(filter.getDateTo()))
                    .collect(Collectors.toList());
        }
        return events;
    }

    @Override
    public void add(Event event) {
        Club club = clubService.getClubById(event.getClub().getId());
        Instructor instructor = instructorService.getById(event.getOrganizer().getId());
        event.setClub(club);
        event.setOrganizer(instructor);
        List<Event> presentEvents = eventRepository.findAll();
        for (Event presentEvent : presentEvents) {
            if(presentEvent.getClub().getCity().equals(event.getClub().getCity()) &&
                    !StringUtils.isEmpty(presentEvent.getClub().getStreet()) &&
                        presentEvent.getClub().getStreet().equals(event.getClub().getStreet()) &&
                        presentEvent.getDate().equals(event.getDate())) {
                throw new DuplicateException("По адресу: " +  event.getClub().getCity() + " " + event.getClub().getStreet() +
                        "на данное время уже забронировано событие " + presentEvent.getTitle());
            }
        }
        eventRepository.save(event);
    }

    @Override
    public void delete(int id) {
        eventRepository.deleteById(id);

    }

    @Override
    public Event findEventById(int id) {
        return eventRepository.findById(id).orElseThrow((() -> new RuntimeException("can't find event")));
    }

    @Override
    public void go(int id) {
        eventRepository.go(id);
    }

    @Override
    public void maybe(int id) {
        eventRepository.maybe(id);
    }
}