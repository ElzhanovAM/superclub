package ru.home.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import ru.home.entity.Instructor;

import java.util.List;

public interface InstructorService {

   List<Instructor> getAll();

    void add(Instructor instructor);

    void delete(int id);

    Instructor getById(int id);

}
