package ru.home.service;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.home.exception.InvalidUploadFileTypeException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UploadServiceImpl implements UploadService {

    private final static String ROOT = "C:/work/superclub/images/";

    public String uploadPoster(MultipartFile file) {
        if (file == null) return null;
        switch (file.getContentType()) {
            case MediaType.IMAGE_PNG_VALUE:
                try {
                    createFile(file);
                } catch (Exception e) {
                    throw new RuntimeException("FAIL!");
                }
                break;
            case MediaType.IMAGE_JPEG_VALUE:
                try {
                    createFile(file);
                } catch (Exception e) {
                    throw new RuntimeException("FAIL!");
                }
                break;
            default:
                throw new InvalidUploadFileTypeException();
        }
        return file.getName() + "/" + file.getOriginalFilename();
    }

    private void createFile(MultipartFile file) throws IOException {
        Path rootLocation = Paths.get(ROOT);
        rootLocation = rootLocation.resolve(file.getName());
        createFolder(rootLocation.toString());
        rootLocation = rootLocation.resolve(file.getOriginalFilename());
        Files.deleteIfExists(rootLocation);
        Files.copy(file.getInputStream(), rootLocation);
    }

    private void createFolder(String path) {
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdir();
        }
    }
}
