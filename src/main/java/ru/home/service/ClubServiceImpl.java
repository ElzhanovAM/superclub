package ru.home.service;


import org.springframework.stereotype.Service;

import ru.home.entity.Club;
import ru.home.exception.DuplicateException;

import ru.home.repository.ClubRepository;

import java.util.List;

@Service
public class ClubServiceImpl implements ClubService {

    private ClubRepository clubRepository;

    public ClubServiceImpl(ClubRepository clubRepository) {
        this.clubRepository = clubRepository;
    }

    @Override
    public void addClub(Club club) {
        List<Club> clubs = clubRepository.findAll();
        for (Club presentClub : clubs) {
            if (presentClub.getName().equals(club.getName())){
                throw new DuplicateException("Клуб с таким названием уже существует");
            }
        }
        clubRepository.save(club);
    }

    @Override
    public List<Club> getAllClubs() {
        return clubRepository.findAll();
    }

    @Override
    public Club getClubById(int id) {
        return clubRepository.findById(id).orElseThrow((() -> new RuntimeException("can't find club")));
    }

    @Override
    public void deleteClub(int id) {
        clubRepository.deleteById(id);
    }
}
