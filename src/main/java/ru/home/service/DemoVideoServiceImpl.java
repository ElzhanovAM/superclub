package ru.home.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.home.entity.DemoVideo;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.filter.Filter;
import ru.home.repository.DemoVideoRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DemoVideoServiceImpl implements DemoVideoService {

    private DemoVideoRepository demoVideoRepository;

    private InstructorService instructorService;

    private EventService eventService;

    public DemoVideoServiceImpl(DemoVideoRepository demoVideoRepository) {
        this.demoVideoRepository = demoVideoRepository;
    }

    @Autowired
    public void setInstructorService(InstructorService instructorService) {
        this.instructorService = instructorService;
    }

    @Autowired
    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @Override
    public List<DemoVideo> getAllVideos(Filter filter) {
        List<DemoVideo> demoVideos = demoVideoRepository.findAll();
        if (!StringUtils.isEmpty(filter.getInstructor())) {
            demoVideos = demoVideos
                    .stream()
                    .filter(a -> a.getAuthor().getFullName().equals(filter.getInstructor()))
                    .collect(Collectors.toList());
        }
        return demoVideos.stream()
                .sorted((a,b) -> b.getLikes()-a.getLikes())
                .collect(Collectors.toList());
    }

    @Override
    public DemoVideo getVideoById(int id) {
        return demoVideoRepository.findById(id).orElseThrow((() -> new RuntimeException("can't find video")));
    }

    @Override
    public void addVideo(DemoVideo demoVideo) {
        Instructor instructor= instructorService.getById(demoVideo.getAuthor().getId());
        demoVideo.setAuthor(instructor);
        if (demoVideo.getEvent() != null) {
            Event event = eventService.findEventById(demoVideo.getEvent().getId());
            demoVideo.setEvent(event);
        }
        demoVideoRepository.save(demoVideo);
    }

    @Override
    public void deleteVideo(int id) {
        demoVideoRepository.deleteById(id);
    }

    @Override
    public void like(int id) {
        demoVideoRepository.like(id);
    }

    @Override
    public void dislike(int id) {
        demoVideoRepository.dislike(id);
    }
}