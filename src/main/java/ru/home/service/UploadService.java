package ru.home.service;


import org.springframework.web.multipart.MultipartFile;


public interface UploadService {

    String uploadPoster(MultipartFile file);
}
