package ru.home.service;

import ru.home.entity.Event;
import ru.home.filter.EventFilter;

import java.util.List;

public interface EventService {
    List<Event> findEvents(EventFilter eventFilter);
    void add(Event event);
    void delete(int id);
    Event findEventById(int id);

    void go(int id);

    void maybe(int id);
}