package ru.home.service;

import ru.home.entity.DemoVideo;
import ru.home.filter.Filter;

import java.util.List;

public interface DemoVideoService {

    List<DemoVideo> getAllVideos(Filter filter);

    DemoVideo getVideoById(int id);

    void addVideo(DemoVideo demoVideo);

    void deleteVideo(int id);

    void like(int id);

    void dislike(int id);
}