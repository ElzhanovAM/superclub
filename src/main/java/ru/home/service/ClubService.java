package ru.home.service;


import org.springframework.web.multipart.MultipartFile;
import ru.home.entity.Club;

import java.util.List;

public interface ClubService {

    void addClub(Club club);

    List<Club> getAllClubs();

    Club getClubById(int id);

    void deleteClub(int id);

}
