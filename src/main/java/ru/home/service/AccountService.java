package ru.home.service;

import ru.home.entity.Account;
import ru.home.security.JwtAuthenticationRequest;

public interface AccountService {
    int saveAccount(JwtAuthenticationRequest request);

    Account getAccountById(int id);

    void deleteAccount(int id);
}
