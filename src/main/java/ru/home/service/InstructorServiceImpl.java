package ru.home.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.home.entity.Account;
import ru.home.entity.Club;
import ru.home.entity.Instructor;
import ru.home.exception.DuplicateException;
import ru.home.repository.InstructorRepository;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InstructorServiceImpl implements InstructorService {

    private InstructorRepository instructorRepository;
    private ClubService clubService;
    private AccountService accountService;

    public InstructorServiceImpl(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }

    @Autowired
    public void setClubService(ClubService clubService) {
        this.clubService = clubService;
    }

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public List<Instructor> getAll() {
        List<Instructor>  instructors = instructorRepository.findAll()
                                    .stream()
                                    .sorted((a, b) -> (int)(b.skill() - a.skill()))
                                    .collect(Collectors.toList());
        return instructors;
    }

    @Override
    public void add(Instructor instructor) {
        List<Instructor> presentInstructors = instructorRepository.findAll();
        for (Instructor presentInstructor : presentInstructors) {
            if(presentInstructor.getFullName().equals(instructor.getFullName())) {
                throw new DuplicateException("Инструктор c именем " + instructor.getFullName() + " уже существует");
            }
            if(!StringUtils.isEmpty(presentInstructor.getVkProfile()) &&
                    presentInstructor.getVkProfile().equals(instructor.getVkProfile())) {
                throw new DuplicateException("Найден иснтруктор с тем же профилем: " + instructor.getVkProfile() +
                        " под именем: " + instructor.getFullName());
            }
        }
        Club instructorClub = clubService.getClubById(instructor.getClub().getId());
        instructor.setClub(instructorClub);

        Account userAccount = accountService.getAccountById(instructor.getAccount().getId());
        instructor.setAccount(userAccount);

        instructorRepository.save(instructor);
    }

    @Override
    public void delete(int id) {
        instructorRepository.deleteById(id);
    }

    @Override
    public Instructor getById(int id) {
        return instructorRepository.findById(id).orElseThrow((() -> new RuntimeException("can't find instructor")));
    }
}