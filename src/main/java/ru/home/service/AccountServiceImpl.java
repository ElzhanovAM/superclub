package ru.home.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.home.entity.Account;
import ru.home.repository.AccountRepository;
import ru.home.security.JwtAuthenticationRequest;

import java.util.List;

@Service
public class AccountServiceImpl implements UserDetailsService, AccountService {
  private final AccountRepository accountRepository;
  private PasswordEncoder passwordEncoder;

  public AccountServiceImpl(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Autowired
  public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    return accountRepository
        .findAccountByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException(username));
  }

  @Override
  public int saveAccount(JwtAuthenticationRequest request) {
    Account account = new Account(
            request.getUsername(),
            passwordEncoder.encode(request.getPassword()),
            List.of(
                    new SimpleGrantedAuthority("VIEW"),
                    new SimpleGrantedAuthority("EDIT")
            ),
            true,
            true,
            true,
            true
    );
    return accountRepository.save(account).getId();
  }

  @Override
  public Account getAccountById(int id) {
    return accountRepository.findById(id).orElseThrow((() -> new RuntimeException("can't find account")));
  }

    @Override
    public void deleteAccount(int id) {
        accountRepository.deleteById(id);
    }
}
