package ru.home.view;

public class View {
    public interface InstructorFullView extends InstructorLightView, ClubLightView, EventLightView, AccountView  {}
    public interface InstructorLightView extends ClubNameView {}
    public interface ClubFullView extends ClubLightView, InstructorLightView, EventLightView {}
    public interface ClubLightView extends ClubNameView {}
    public interface ClubNameView{}
    public interface EventFullView extends EventLightView, InstructorLightView, ClubLightView{}
    public interface EventLightView {}
    public interface VideoFullView extends VideoLightView{}
    public interface VideoLightView {}

    public interface AccountView {}
}