package ru.home.controller;


import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import ru.home.security.JwtAuthenticationRequest;
import ru.home.service.AccountService;



@CrossOrigin
@RestController
@RequestMapping("/api/account")
public class AccountController {
    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }


    @PostMapping
    public int saveAccount(@RequestBody JwtAuthenticationRequest request) {
        return accountService.saveAccount(request);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Remove account by Id")
    public void deleteAccount(@PathVariable int id) {
        accountService.deleteAccount(id);
    }
}
