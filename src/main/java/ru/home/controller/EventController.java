package ru.home.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.home.entity.Event;
import ru.home.exception.RestValidationException;
import ru.home.filter.EventFilter;
import ru.home.service.EventService;
import ru.home.service.UploadService;
import ru.home.view.View;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/events")
public class EventController {
    private EventService eventService;

    private UploadService uploadService;

    public EventController(EventService eventService, UploadService uploadService) {
        this.eventService = eventService;
        this.uploadService = uploadService;
    }

    @PostMapping
    @ApiOperation("Find list of Events")
    @JsonView(View.EventLightView.class)
    public List<Event> findEvents(@RequestBody EventFilter eventFilter){
        return eventService.findEvents(eventFilter);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get event by Id")
    @JsonView(View.EventFullView.class)
    public Event findEventById(@PathVariable int id) {
        return eventService.findEventById(id);
    }

    @PostMapping("/add")
    @ApiOperation("Add event")
    public void addEvents(@RequestBody @Valid Event event, BindingResult bindingResult){
        if (bindingResult.hasErrors()) {
            throw new RestValidationException(bindingResult.getFieldError().getDefaultMessage());
        }
        eventService.add(event);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Remove event by Id")
    public void delete(@PathVariable int id){
        eventService.delete(id);
    }

    @PutMapping("/{id}/go")
    @ApiOperation("Go to the event")
    public void go(@PathVariable int id) {
        eventService.go(id);
    }

    @PutMapping("/{id}/maybe")
    @ApiOperation("MayBe go to the event")
    public void mayBe(@PathVariable int id) {
        eventService.maybe(id);
    }

    @PostMapping("/poster")
    @ApiOperation("Load Event Poster")
    @ApiIgnore
    public String uploadEventPoster(@RequestParam("eventPoster") MultipartFile file) {
        return uploadService.uploadPoster(file);
    }
}
