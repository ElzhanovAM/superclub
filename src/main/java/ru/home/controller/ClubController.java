package ru.home.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.home.entity.Club;
import ru.home.exception.RestValidationException;
import ru.home.service.ClubService;
import ru.home.service.UploadService;
import ru.home.view.View;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/clubs")
public class ClubController {
    private ClubService clubService;
    private UploadService uploadService;

    public ClubController(ClubService clubService, UploadService uploadService) {
        this.clubService = clubService;
        this.uploadService = uploadService;
    }

    @PostMapping
    @ApiOperation("Add new Club")
    public void addClub(@RequestBody @Valid Club club, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new RestValidationException(bindingResult.getFieldError().getDefaultMessage());
        }
        clubService.addClub(club);
    }

    @PostMapping("/poster")
    @ApiOperation("Load Club Poster")
    @ApiIgnore
    public String uploadClubPoster(@RequestParam("clubPoster") MultipartFile file) {
        return uploadService.uploadPoster(file);
    }

    @GetMapping
    @ApiOperation("Get list of Clubs")
    @JsonView(View.ClubLightView.class)
    public List<Club> getAllClubs() {
        return clubService.getAllClubs();
    }

    @GetMapping("/{id}")
    @ApiOperation("Get club by Id")
    @JsonView(View.ClubFullView.class)
    public Club getClubById(@PathVariable int id) {
        return clubService.getClubById(id);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Remove club by Id")
    public void deleteClub(@PathVariable int id) {
        clubService.deleteClub(id);
    }
}
