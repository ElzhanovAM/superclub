package ru.home.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import ru.home.entity.DemoVideo;
import ru.home.filter.Filter;
import ru.home.service.DemoVideoService;
import ru.home.view.View;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/videos")
@ApiIgnore
//TODO add demoVideo classes to next release
public class DemoVideoController {

    private DemoVideoService demoVideoService;

    public DemoVideoController(DemoVideoService demoVideoService) {
        this.demoVideoService = demoVideoService;
    }

    @PostMapping
    @ApiOperation("Get list of Videos")
    @JsonView(View.VideoFullView.class)
    public List<DemoVideo> getAllVideos(@RequestBody Filter filter) {
        return demoVideoService.getAllVideos(filter);
    }

    @GetMapping("/{id}")
    @ApiOperation("Get video by Id")
    @JsonView(View.VideoLightView.class)
    public DemoVideo getVideoById(@PathVariable int id) {
        return demoVideoService.getVideoById(id);
    }

    @PostMapping("/add")
    @ApiOperation("Add video")
    public void addVideo(@RequestBody DemoVideo demoVideo) {
        demoVideoService.addVideo(demoVideo);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete video")
    public void deleteVideo(@PathVariable int id) {
        demoVideoService.deleteVideo(id);
    }

    @PutMapping("/{id}/like")
    @ApiOperation("Like video")
    public void like (@PathVariable int id) {
        demoVideoService.like(id);
    }

    @PutMapping("/{id}/dislike")
    @ApiOperation("Dislike video")
    public void dislike (@PathVariable int id) {
        demoVideoService.dislike(id);
    }
}
