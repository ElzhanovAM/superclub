package ru.home.controller;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.home.entity.Instructor;
import ru.home.exception.RestValidationException;
import ru.home.service.InstructorService;
import ru.home.service.UploadService;
import ru.home.view.View;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/instructors")
public class InstructorController {

    private InstructorService instructorService;

    private UploadService uploadService;

    public InstructorController(InstructorService instructorService, UploadService uploadService) {
        this.instructorService = instructorService;
        this.uploadService = uploadService;
    }

    @GetMapping
    @ApiOperation("Get list of Instructors")
    @JsonView(View.InstructorLightView.class)
    public List<Instructor> getAll() {
        return instructorService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation("Get instructor by Id")
    @JsonView(View.InstructorFullView.class)
    public Instructor getById(@PathVariable int id) {
        return instructorService.getById(id);
    }


    @PostMapping
    @ApiOperation("Add new Instructor")
    public void add(@RequestBody @Valid Instructor instructor, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new RestValidationException(bindingResult.getFieldError().getDefaultMessage());
        }
        instructorService.add(instructor);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Remove instructor by Id")
    public void delete (@PathVariable int id){
        instructorService.delete(id);
    }

    @PostMapping("/poster")
    @ApiOperation("Load Instructor Poster")
    @ApiIgnore
    public String uploadInstructorAvatar(@RequestParam("instructorAvatar") MultipartFile file) {
        return uploadService.uploadPoster(file);
    }
}
