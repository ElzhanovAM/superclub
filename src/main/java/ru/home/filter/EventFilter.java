package ru.home.filter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventFilter extends Filter {
    String city;
    Date dateFrom;
    Date dateTo;

    public EventFilter(String instructor, String city, Date dateFrom, Date dateTo) {
        super(instructor);
        this.city = city;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }
}