package ru.home.entity;


import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.home.deserializer.CustomDateDeserializer;
import ru.home.serializer.CustomDateSerializer;
import ru.home.validator.VkProfileValueValidBeanConstraint;
import ru.home.view.View;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonView(View.InstructorFullView.class)
public class Instructor implements Serializable{
    @Id
    @GeneratedValue
    @JsonView(View.InstructorLightView.class)
    private int id;
    @JsonView(View.InstructorLightView.class)
    @NotNull(message="У инструктора должно быть имя")
    @Size(min = 3, max = 40)
    private String fullName;
    @Min(value = 16, message="Инструктор должен быть совершеннолетним")
    private int age;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date startTraining;
    @JsonView(View.InstructorLightView.class)
    private String avatar;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> regalia;
    @ManyToOne
    @NotNull(message = "Пожалуйста, укажите клуб")
    private Club club;
    @OneToMany(mappedBy = "organizer", cascade = CascadeType.ALL)
    private List<Event> events;
    @Column(nullable = false)
    @NotNull(message = "Пожалуйста, укажите опыт")
    private int experience;
    @VkProfileValueValidBeanConstraint(message = "Данная ссылка не может существовать")
    private String vkProfile;
    @Column(nullable = false)
    @JsonView(View.InstructorLightView.class)
    @NotNull(message = "Пожалуйста, укажите телефон")
    private String phone;
    @OneToOne(cascade = CascadeType.ALL)
    private Account account;

    public Instructor(@NotNull(message = "У инструктора должно быть имя") @Size(min = 3, max = 40) String fullName) {
        this.fullName = fullName;
    }

    public Instructor(int id, @NotNull(message = "У инструктора должно быть имя") @Size(min = 3, max = 40) String fullName) {
        this.id = id;
        this.fullName = fullName;
    }

    public Instructor(String fullName, int age,  String avatar, List<String> regalia, int experience, String phone) {
        this.fullName = fullName;
        this.age = age;
        this.avatar = avatar;
        this.regalia = regalia;
        this.experience = experience;
        this.phone = phone;
    }

    public Instructor(String fullName, int age, Date startTraining, String avatar, List<String> regalia, int experience) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.experience = experience;
    }

    public Instructor(int id, String fullName, int age, Date startTraining, String avatar, List<String> regalia, List<Event> events, int experience) {
        this.id = id;
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.events = events;
        this.experience = experience;
    }

    public Instructor(String fullName, int age, Date startTraining, String avatar, List<String> regalia, List<Event> events, int experience, String phone) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.events = events;
        this.experience = experience;
        this.phone = phone;
    }

    public Instructor(String fullName, int age, Date startTraining, String avatar, List<String> regalia, List<Event> events, int experience, String phone, Account account) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.events = events;
        this.experience = experience;
        this.phone = phone;
        this.account = account;
    }

    public Instructor(String fullName, int age, Date startTraining, String avatar, List<String> regalia, List<Event> events, int experience) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.events = events;
        this.experience = experience;
    }

    public Instructor(@NotNull(message = "У инструктора должно быть имя") @Size(min = 3, max = 40) String fullName, int age, Date startTraining, String avatar, List<String> regalia, @NotNull(message = "Пожалуйста, укажите опыт") int experience, String vkProfile) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.experience = experience;
        this.vkProfile = vkProfile;
    }

    public Instructor(@NotNull(message = "У инструктора должно быть имя") @Size(min = 3, max = 40) String fullName, int age, Date startTraining, String avatar, List<String> regalia, @NotNull(message = "Пожалуйста, укажите опыт") int experience, String vkProfile, String phone) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.experience = experience;
        this.vkProfile = vkProfile;
        this.phone = phone;
    }

    public Instructor(@NotNull(message = "У инструктора должно быть имя") @Size(min = 3, max = 40) String fullName, int age, Date startTraining, String avatar, List<String> regalia,List<Event> events, @NotNull(message = "Пожалуйста, укажите опыт") int experience, String vkProfile, String phone) {
        this.fullName = fullName;
        this.age = age;
        this.startTraining = startTraining;
        this.avatar = avatar;
        this.regalia = regalia;
        this.events = events;
        this.experience = experience;
        this.vkProfile = vkProfile;
        this.phone = phone;
    }



    public long skill() {
        Date date = new Date();
        Calendar firstDayOfCurrentYear = Calendar.getInstance();
        firstDayOfCurrentYear.setTime(date);
        firstDayOfCurrentYear.set(Calendar.MONTH, 0);
        firstDayOfCurrentYear.set(Calendar.DAY_OF_MONTH, 1);
        firstDayOfCurrentYear.set(Calendar.HOUR_OF_DAY, 0);
        firstDayOfCurrentYear.set(Calendar.MINUTE, 0);
        firstDayOfCurrentYear.set(Calendar.SECOND, 0);
        Date currentYearDate = firstDayOfCurrentYear.getTime();

        int eventCount = 1 + events.stream().filter(a -> a.getDate().after(currentYearDate))
                .collect(Collectors.toList()).size();
        return experience * eventCount;
    }
}
