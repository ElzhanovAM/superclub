package ru.home.entity;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.home.view.View;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account implements UserDetails, Serializable {
  @Id
  @GeneratedValue
  @JsonView(View.AccountView.class)
  private int id;
  @Column(nullable = false, unique = true)
  @JsonView(View.AccountView.class)
  private String username;
  @Column(nullable = false)
  private String password;
  @ElementCollection(fetch = FetchType.EAGER)
  @Fetch(value = FetchMode.SUBSELECT)
  private Collection<GrantedAuthority> authorities;
  private boolean enabled;
  private boolean accountNonExpired;
  private boolean accountNonLocked;
  private boolean credentialsNonExpired;
  private Date lastPasswordResetDate;

  public boolean hasAuthority(String authority) {
    return authorities
        .stream()
        .anyMatch(o -> o.getAuthority().equals(authority));
  }

  public Account(String username, String password, Collection<GrantedAuthority> authorities, boolean enabled, boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired) {
    this.username = username;
    this.password = password;
    this.authorities = authorities;
    this.enabled = enabled;
    this.accountNonExpired = accountNonExpired;
    this.accountNonLocked = accountNonLocked;
    this.credentialsNonExpired = credentialsNonExpired;
  }
}
