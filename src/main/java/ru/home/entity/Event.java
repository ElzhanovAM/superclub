package ru.home.entity;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.home.deserializer.CustomDateDeserializer;
import ru.home.serializer.CustomDateSerializer;
import ru.home.view.View;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonView(View.EventFullView.class)
public class Event {
    @Id
    @GeneratedValue
    @JsonView(View.EventLightView.class)
    private int id;
    @JsonView(View.EventLightView.class)
    @Size(min = 3, max = 40)
    private String title;
    @ManyToOne
    @NotNull(message="У события должен быть организатор")
    private Instructor organizer = new Instructor();
    @ManyToOne
    @NotNull(message="Установите клуб")
    private Club club;
    private String description;
    @JsonView(View.EventLightView.class)
    @NotNull(message="Установите дату события")
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date date;
    @JsonView(View.EventLightView.class)
    private String poster;
    @JsonView(View.EventLightView.class)
    private int go;
    @JsonView(View.EventLightView.class)
    private int mayBe;

    public Event(String title, String description, Date date, String poster) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.poster = poster;
    }

    public Event(String title, Instructor organizer, Club club, Date date) {
        this.title = title;
        this.organizer = organizer;
        this.club = club;
        this.date = date;
    }

    public Event(int id, String title, Instructor organizer, Club club, Date date) {
        this.id = id;
        this.title = title;
        this.organizer = organizer;
        this.club = club;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", organizer=" + organizer.getFullName() +
                ", address=" + club  +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", poster='" + poster + '\'' +
                ", go=" + go +
                ", mayBe=" + mayBe +
                '}';
    }
}
