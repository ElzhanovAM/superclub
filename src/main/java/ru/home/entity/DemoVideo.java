package ru.home.entity;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.home.view.View;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonView(View.VideoFullView.class)
public class DemoVideo {
    @Id
    @GeneratedValue
    @JsonView(View.VideoLightView.class)
    private int id;
    @JsonView(View.VideoLightView.class)
    @NotNull(message="У видео должно быть название")
    private String title;
    private String link;
    @ManyToOne
    private Instructor author;
    @ManyToOne
    private Event event;
    private int likes;
}
