package ru.home.entity;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.home.view.View;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonView(View.ClubFullView.class)
public class Club {
    @Id
    @GeneratedValue
    @JsonView(View.ClubLightView.class)
    private int id;
    @JsonView(View.ClubNameView.class)
    @NotEmpty(message = "У клуба должно быть название")
    private String name;
    @JsonView(View.ClubLightView.class)
    @NotEmpty(message = "Пожалуйста, укажите город, клуба")
    private String city;
    @JsonView(View.ClubLightView.class)
    private String street;
    @OneToMany(mappedBy = "club", cascade = CascadeType.ALL)
    private List<Event> events = new ArrayList<>();
    @OneToMany(mappedBy = "club", cascade = CascadeType.ALL)
    private List<Instructor> instructors = new ArrayList<>();
    @JsonView(View.ClubLightView.class)
    private String poster;

    public Club(@NotNull(message = "У клуба должно быть название") String name, @NotNull(message = "Пожалуйста, укажите город, клуба") String city, String street) {
        this.name = name;
        this.city = city;
        this.street = street;
    }

    public Club(int id, @NotNull(message = "У клуба должно быть название") String name, @NotNull(message = "Пожалуйста, укажите город, клуба") String city, String street) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.street = street;
    }
}
