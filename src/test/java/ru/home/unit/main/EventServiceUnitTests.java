package ru.home.unit.main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.home.entity.Club;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.exception.DuplicateException;
import ru.home.filter.EventFilter;
import ru.home.repository.EventRepository;
import ru.home.service.ClubService;
import ru.home.service.EventService;
import ru.home.service.EventServiceImpl;
import ru.home.service.InstructorService;

import java.util.Date;
import java.util.List;

public class EventServiceUnitTests {

    @Test
    void testFindAllEvents() {
        EventRepository eventRepository = Mockito.mock(EventRepository.class);

        Event firstEvent = new Event("Seminar", new Instructor("Org1"),
                new Club("KazanClub","Kazan", "someStreet"), new Date());
        Event secondEvent = new Event("Open champ", new Instructor("Org2"),
                new Club("MoscowClub","Moscow", "noStreet"), new Date());

        Mockito.when(eventRepository.findAll())
                .thenReturn(
                        List.of(firstEvent,secondEvent)
                );
        EventService eventService = new EventServiceImpl(eventRepository);

        List<Event> result = eventService.findEvents(new EventFilter("Org1",null, null, null));

        Assertions.assertEquals(
                1,
                result.size()
        );

        Assertions.assertEquals(
                "Seminar",
                result.get(0).getTitle()
        );

        result = eventService.findEvents(new EventFilter(null,"Moscow", null, null ));

        Assertions.assertEquals(
                1,
                result.size()
        );

        Assertions.assertEquals(
                "Open champ",
                result.get(0).getTitle()
        );

        Assertions.assertEquals(
                0,
                eventService.findEvents(new EventFilter(null,null, new Date(), null )).size()
        );

        Assertions.assertEquals(
                2,
                eventService.findEvents(new EventFilter(null,null, null, new Date())).size()
        );
    }

    @Test
    void testAddEvent() {
        EventRepository eventRepository = Mockito.mock(EventRepository.class);
        ClubService clubService = Mockito.mock(ClubService.class);
        InstructorService instructorService = Mockito.mock(InstructorService.class);
        Club presentClub = new Club(1, "KazanClub1", "Kazan", "someStreet");
        Club addedClub = new Club(2, "KazanClub2", "Kazan", "someStreet");
        Instructor firstInstructor = new Instructor(1,"Org1");
        Instructor secondInstructor = new Instructor(1,"Org2");
        Event firstEvent = new Event("Seminar", firstInstructor,
                presentClub, new Date(1000));
        Event secondEvent = new Event("Training", secondInstructor,
                addedClub, new Date(1000));

        Mockito.when(eventRepository.findAll())
                .thenReturn(
                        List.of(firstEvent)
                );

        Mockito.when(clubService.getClubById(addedClub.getId()))
                .thenReturn(
                        addedClub
                );

        Mockito.when(instructorService.getById(secondInstructor.getId()))
                .thenReturn(
                        secondInstructor
                );

        Mockito.when(eventRepository.findAll())
                .thenReturn(
                        List.of(firstEvent)
                );

        Mockito.when(eventRepository.save(secondEvent))
                .thenReturn(
                        secondEvent
                );

        EventServiceImpl eventService = new EventServiceImpl(eventRepository);
        eventService.setInstructorService(instructorService);
        eventService.setClubService(clubService);

        Assertions.assertThrows(
                DuplicateException.class,
                () -> eventService.add(secondEvent)
        );
    }
}
