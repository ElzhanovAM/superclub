package ru.home.unit.main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.exception.DuplicateException;
import ru.home.repository.InstructorRepository;
import ru.home.service.InstructorService;
import ru.home.service.InstructorServiceImpl;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class InstructorServiceUnitTests {

    @Test
    void testFindAllInstructors() {
        InstructorRepository instructorRepository = Mockito.mock(InstructorRepository.class);

        List<Event> events = List.of(
                new Event("Seminar", "seminar of Artur", new Date(), "photo"),
                new Event("Open champ", "Open championship", new Date(), "photo")
        );
        Instructor firstInstructor = new Instructor(
                "first",
                43,
                new Date(),
                "first.png",
                List.of("instructor", "representer"),
                Collections.singletonList(events.get(0)),
                5);

        Instructor secondInstructor = new Instructor(
                "second",
                33,
                new Date(),
                "second.png",
                List.of("instructor"),
                events,
                5);

        Instructor thirdInstructor = new Instructor(
                "third",
                23,
                new Date(),
                "third.png",
                List.of("instructor"),
                Collections.emptyList(),
                3);

        Mockito.when(instructorRepository.findAll())
                .thenReturn(
                        List.of(firstInstructor,secondInstructor,thirdInstructor)
                );

        InstructorService instructorService = new InstructorServiceImpl(
                instructorRepository
        );


        List<Instructor> instructors = instructorService.getAll();
        Assertions.assertEquals(
                3,
                instructors.size()
        );

        Assertions.assertEquals(
                secondInstructor,
                instructors.get(0)
        );

        Assertions.assertEquals(
                firstInstructor,
                instructors.get(1)
        );

        Assertions.assertEquals(
                thirdInstructor,
                instructors.get(2)
        );


        Mockito.verify(
                instructorRepository,
                Mockito.times(1)
        ).findAll();
        Mockito.verifyNoMoreInteractions(instructorRepository);
    }

    @Test
    void testAddInstructor() {
        InstructorRepository instructorRepository = Mockito.mock(InstructorRepository.class);

        Instructor instructor = new Instructor(
                "first",
                43,
                new Date(),
                "first.png",
                List.of("instructor", "representer"),
                5,
                "http//vk.com/first");

        Instructor instructorWithTheSameName = new Instructor(
                "first",
                33,
                new Date(),
                "second.png",
                List.of("instructor"),
                5,
                "http//vk.ru/first");

        Instructor instructorWithTheSameProfile = new Instructor(
                "second",
                23,
                new Date(),
                "third.png",
                List.of("instructor"),
                3,
                "http//vk.com/first");

        Mockito.when(instructorRepository.findAll())
                .thenReturn(
                        List.of(instructor)
                );

        Mockito.when(instructorRepository.save(instructorWithTheSameName))
                .thenReturn(instructorWithTheSameName);

        Mockito.when(instructorRepository.save(instructorWithTheSameProfile))
                .thenReturn(instructorWithTheSameProfile);

        InstructorService instructorService = new InstructorServiceImpl(
                instructorRepository
        );

        Assertions.assertThrows(
                DuplicateException.class,
                () -> instructorService.add(instructorWithTheSameName)
        );

        Assertions.assertThrows(
                DuplicateException.class,
                () -> instructorService.add(instructorWithTheSameProfile)
        );
    }
}
