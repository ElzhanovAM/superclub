package ru.home.e2e.main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.home.entity.Club;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.repository.ClubRepository;
import ru.home.repository.InstructorRepository;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TestRestTemplateSpringBootTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private ClubRepository clubRepository;

	@Autowired
	private InstructorRepository instructorRepository;

	@BeforeEach
	void initData() {
		Club club = new Club("FCS Kazan","Kazan", "Hadi Taktash");
		clubRepository.save(club);
		List<Club> clubs = clubRepository.findAll();
		List<Event> events = List.of(
				new Event("Seminar", "seminar of Artur", new Date(), "photo"),
				new Event("Open champ", "Open championship", new Date(), "photo")
		);

		Instructor instructor = new Instructor(
				"Artur",
				33,
				new Date(),
				"photo",
				List.of("instructor", "representer"),
				events,
				5,
				"http://vk.com/instructor",
				"87774446677");
		instructor.setClub(clubs.get(0));


		for (Event event: events) {
			event.setClub(club);
			event.setOrganizer(instructor);
		}

		instructorRepository.save(instructor);
	}


	@Test
	@DirtiesContext
	void testRestGetAll() {
		List <Club> clubs = restTemplate.exchange("/api/clubs", HttpMethod.GET, null, new ParameterizedTypeReference<List<Club>>() {}).getBody();
		assertEquals(1, clubs.size());
		Club club = clubs.get(0);
		assertEquals(0, club.getInstructors().size());
		assertEquals(0, club.getEvents().size());
	}

	@Test
	@DirtiesContext
	void testRestGetById() {
		Club club = restTemplate.exchange("/api/clubs/{id}", HttpMethod.GET, null, new ParameterizedTypeReference<Club>() {}, 1).getBody();
		assertEquals("FCS Kazan", club.getName());
		assertEquals("Kazan", club.getCity());
		assertEquals("Hadi Taktash", club.getStreet());
		assertEquals(2, club.getEvents().size());

		Instructor instructor = club.getInstructors().get(0);
		assertEquals("Artur", instructor.getFullName());
	}

	@Test
	@DirtiesContext
	void testRestAdd() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		List <Club> clubs = restTemplate.exchange("/api/clubs", HttpMethod.GET, null, new ParameterizedTypeReference<List<Club>>() {}).getBody();

		Instructor instructor = new Instructor(
				"Ruslan",
				43,
				new Date(),
				"photo",
				List.of("representer"),
				10,
				"http://vk.com/instructorOne",
				"87774446677");
		instructor.setClub(clubs.get(0));

		HttpEntity<Instructor> entity = new HttpEntity<>(instructor, headers);

		ResponseEntity<Object> response = restTemplate.exchange("/api/instructors", HttpMethod.POST, entity, Object.class);

		assertEquals(HttpStatus.OK, response.getStatusCode());

		List<Instructor> instructors = restTemplate.exchange("/api/instructors", HttpMethod.GET, null, new ParameterizedTypeReference<List<Instructor>>() {}).getBody();
		assertEquals(2, instructors.size());
	}

	@Test
	@DirtiesContext
	void testRestAddInstructorWithExistProfile() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		List <Club> clubs = restTemplate.exchange("/api/clubs", HttpMethod.GET, null, new ParameterizedTypeReference<List<Club>>() {}).getBody();
		Instructor instructor = new Instructor(
				"Ruslan",
				43,
				new Date(),
				"photo",
				List.of("representer"),
				10,
				"http://vk.com/instructor",
				"87774446677");
		instructor.setClub(clubs.get(0));

		HttpEntity<Instructor> entity = new HttpEntity<>(instructor, headers);

		ResponseEntity<Object> response = restTemplate.exchange("/api/instructors", HttpMethod.POST, entity, Object.class);

		assertEquals(HttpStatus.NOT_ACCEPTABLE, response.getStatusCode());
	}

	@Test
	@DirtiesContext
	void testRestAddInstructorWithInvalidProfile() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		List <Club> clubs = restTemplate.exchange("/api/clubs", HttpMethod.GET, null, new ParameterizedTypeReference<List<Club>>() {}).getBody();
		Instructor instructor = new Instructor(
				"Ruslan",
				43,
				new Date(),
				"photo",
				List.of("representer"),
				10,
				"vk.com/profile",
				"87774446677");
		instructor.setClub(clubs.get(0));

		HttpEntity<Instructor> entity = new HttpEntity<>(instructor, headers);

		ResponseEntity<Object> response = restTemplate.exchange("/api/instructors", HttpMethod.POST, entity, Object.class);

		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
}
