package ru.home.integration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.home.controller.InstructorController;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.service.InstructorService;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {InstructorController.class})
public class InstructorControllerMockMvcTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InstructorService instructorService;


    @Test
    public void getAll() throws Exception {
        List<Event> events = List.of(
                new Event("Seminar", "seminar of Artur", new Date(), "photo"),
                new Event("Open champ", "Open championship", new Date(), "photo")
        );
        Instructor firstInstructor = new Instructor(
                1,
                "first",
                43,
                new Date(),
                "first.png",
                List.of("instructor", "representer"),
                Collections.singletonList(events.get(0)),
                5);

        Instructor secondInstructor = new Instructor(
                2,
                "second",
                33,
                new Date(),
                "second.png",
                List.of("instructor"),
                events,
                5);

        when(instructorService.getAll()).thenReturn(List.of(firstInstructor, secondInstructor));

        mockMvc.perform(get("/api/instructors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content()
                        .json("[" +
                                "{\"id\":1," +
                                "\"fullName\":\"first\"," +
                                "\"avatar\":\"first.png\"" +
                                "}," +
                                "{\"id\":2," +
                                "\"fullName\":\"second\"," +
                                "\"avatar\":\"second.png\"" +
                                "}" +
                                "]"))
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void getById() throws Exception {
        List<Event> events = List.of(
                new Event("Seminar", "seminar of Artur", new Date(1000), "photo"),
                new Event("Open champ", "Open championship", new Date(2000), "photo")
        );

        Instructor instructor = new Instructor(
                1,
                "first",
                43,
                new Date(500),
                "first.png",
                List.of("instructor", "representer"),
                events,
                5);

        when(instructorService.getById(1)).thenReturn(instructor);

        mockMvc.perform(get("/api/instructors/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content()
                        .json("{" +
                                "\"id\":1," +
                                "\"fullName\":\"first\"," +
                                "\"age\":43," +
                                "\"startTraining\":\"01.01.1970\"," +
                                "\"avatar\":\"first.png\"," +
                                "\"regalia\":[\"instructor\",\"representer\"]," +
                                "\"club\":null," +
                                "\"events\":[{\"id\":0,\"title\":\"Seminar\",\"date\":\"01.01.1970\"}," +
                                            "{\"id\":0,\"title\":\"Open champ\",\"date\":\"01.01.1970\"}]," +
                                "\"videos\":null," +
                                "\"experience\":5," +
                                "\"vkProfile\":null," +
                                "\"phone\":null}"
                                ));
    }
}
