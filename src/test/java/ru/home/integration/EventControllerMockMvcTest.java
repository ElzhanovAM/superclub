package ru.home.integration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.home.controller.EventController;
import ru.home.entity.Club;
import ru.home.entity.Event;
import ru.home.entity.Instructor;
import ru.home.filter.EventFilter;
import ru.home.service.EventService;

import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = {EventController.class})
public class EventControllerMockMvcTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    @Test
    public void getAll() throws Exception {
        Event firstEvent = new Event(1, "Seminar", new Instructor("Org1"),
                new Club("KazanClub", "Kazan", "someStreet"), new Date(1000));
        Event secondEvent = new Event(2, "Training", new Instructor("Org2"),
                new Club("MoscowClub", "Moscow", "someStreet"), new Date(1000));


        when(eventService.findEvents(new EventFilter(null, null, null, null))).thenReturn(List.of(firstEvent, secondEvent));

        mockMvc.perform(post("/api/events")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{" +
                        "\"instructor\":null," +
                        "\"city\":null," +
                        "\"dateFrom\":null," +
                        "\"dateTo\":null" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content()
                        .json("[" +
                                        "{\"id\":1," +
                                        "\"title\":\"Seminar\"," +
                                        "\"date\":\"01.01.1970\"}," +
                                        "{\"id\":2," +
                                        "\"title\":\"Training\"," +
                                        "\"date\":\"01.01.1970\"}" +
                                "]"))
                .andExpect(jsonPath("$.length()").value(2));
    }

    @Test
    public void getById() throws Exception {
        Event event = new Event(1, "Seminar", new Instructor("Org1"),
                new Club("KazanClub", "Kazan", "someStreet"), new Date(1000));

        when(eventService.findEventById(1)).thenReturn(event);

        mockMvc.perform(get("/api/events/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content()
                        .json("{" +
                                "\"id\":1," +
                                "\"title\":\"Seminar\"," +
                                "\"demoVideos\":null," +
                                "\"description\":null," +
                                "\"date\":\"01.01.1970\"," +
                                "\"poster\":null," +
                                "\"go\":0," +
                                "\"mayBe\":0" +
                                "}"
                        ));
    }
}
