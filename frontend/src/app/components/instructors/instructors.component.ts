import { Component, OnInit } from '@angular/core';
import {BaseInstructor} from "../../domain/base-instructor";
import {InstructorService} from "../../services/instructor.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-instructors',
  templateUrl: './instructors.component.html',
  styleUrls: ['./instructors.component.scss']
})
export class InstructorsComponent implements OnInit {

  instructors : BaseInstructor[] = [];

  isAuth : boolean;

  constructor(
    private instructorService : InstructorService,
    private authService: AuthService
    ) { }

  ngOnInit() {
    this.isAuth = this.authService.isAuth()
    this.load()
  }

  onRemove(id: number) {
    this.instructorService.deleteInstructor(id).subscribe(
      _=>this.load(),
      error => console.log(error)
    );
  }

  onSave() {
    this.load();
  }

  private load() {
    this.instructorService.getInstructors().subscribe(
      data => this.instructors = data,
      error => console.log(error)
    )
  }
}
