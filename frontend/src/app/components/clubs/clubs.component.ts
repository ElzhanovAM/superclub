import { Component, OnInit } from '@angular/core';
import {BaseClub} from "../../domain/base-club";
import {ClubService} from "../../services/club.service";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-clubs',
  templateUrl: './clubs.component.html',
  styleUrls: ['./clubs.component.scss']
})
export class ClubsComponent implements OnInit {
  isAuth : boolean;
  clubs: BaseClub[] = [];
  constructor(private clubService : ClubService,
              private authService: AuthService) { }

  ngOnInit() {
    this.isAuth = this.authService.isAuth()
    this.load()
  }

  onSave() {
    this.load();
  }


  onRemove(id: number) {
    this.clubService.deleteClub(id).subscribe(
      _=>this.load(),
      error => console.log(error)
    );
  }

  private load() {
    this.clubService.getClubs().subscribe(
      data => this.clubs = data,
      error => console.log(error)
    )
  }
}
