import { Component, OnInit } from '@angular/core';
import {ClubService} from "../../services/club.service";
import {Club} from "../../domain/club";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-club-details',
  templateUrl: './club-details.component.html',
  styleUrls: ['./club-details.component.scss']
})
export class ClubDetailsComponent implements OnInit {

  club: Club;
  constructor(private clubService : ClubService,
              private route : ActivatedRoute,
              private router : Router) { }

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get('id'));

    if (isNaN(id)) {
      this.router.navigate(['/']);
      return;
    }

    this.clubService.getClubById(id).subscribe(
      data => this.club = data ,
      error => console.log(error)
    );
  }
}
