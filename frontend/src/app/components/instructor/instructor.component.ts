import { Component, OnInit } from '@angular/core';
import {Instructor} from "../../domain/instructor";
import {InstructorService} from "../../services/instructor.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.html',
  styleUrls: ['./instructor.component.scss']
})
export class InstructorComponent implements OnInit {

  instructor : Instructor;
  constructor(private instructorService : InstructorService,
              private route : ActivatedRoute,
              private router : Router) { }

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get('id'));

    if (isNaN(id)) {
      this.router.navigate(['/']);
      return;
    }

    this.instructorService.getInstructorById(id).subscribe(
      data => this.instructor = data ,
      error => console.log(error)
    );
  }

}
