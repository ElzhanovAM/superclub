import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BaseClub} from "../../domain/base-club";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-club',
  templateUrl: './club.component.html',
  styleUrls: ['./club.component.scss']
})
export class ClubComponent implements OnInit {

  isAuth : boolean;
  @Input() club: BaseClub;

  @Output() remove = new EventEmitter();
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.isAuth = this.authService.isAuth()
  }

  onRemove() {
    this.remove.emit();
  }
}
