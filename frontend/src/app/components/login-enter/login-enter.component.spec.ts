import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginEnterComponent } from './login-enter.component';

describe('LoginEnterComponent', () => {
  let component: LoginEnterComponent;
  let fixture: ComponentFixture<LoginEnterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginEnterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginEnterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
