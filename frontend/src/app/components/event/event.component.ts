import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BaseEvent} from "../../domain/base-event";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  isAuth : boolean;

  @Input() event: BaseEvent;
  @Output() go = new EventEmitter();
  @Output() mayBe = new EventEmitter();
  @Output() remove = new EventEmitter();
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.isAuth = this.authService.isAuth()
  }

  onGo() {
    this.go.emit();
  }

  onMayBe() {
    this.mayBe.emit();
  }

  onRemove() {
    this.remove.emit();
  }
}
