import { Component, OnInit } from '@angular/core';
import {BaseEvent} from "../../domain/base-event";
import {EventService} from "../../services/event.service";
import {EventSearch} from "../../domain/event-search";
import {AuthService} from "../../services/auth.service";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  isAuth : boolean;
  eventSearch : EventSearch = new EventSearch(
    "","", "", "");
  events: BaseEvent[] = [];
  constructor(private eventService : EventService,
              private authService: AuthService) { }

  ngOnInit() {
    this.isAuth = this.authService.isAuth()
    this.load()
  }

  onSave() {
    this.load();
  }

  onGo(id: number) {
    this.eventService.go(id).subscribe(
      _ => this.load(),
      error => console.log(error)
    );
  }

  onMayBe(id: number) {
    this.eventService.mayBe(id).subscribe(
      _ => this.load(),
      error => console.log(error)
    );
  }

  onRemove(id: number) {
    this.eventService.deleteEvent(id).subscribe(
      _=>this.load(),
      error => console.log(error)
    );
  }

  onSearch() {
    this.eventService.getEvents(this.eventSearch).subscribe(
      data => {
        this.events = data;
        this.clearSearch()
      },
          error => console.log(error)

    )
  }

  private clearSearch() {
    this.eventSearch = new EventSearch(
      "","", "", "");
  }

  private load() {
    this.eventService.getEvents(this.eventSearch).subscribe(
      data => this.events = data,
      error => console.log(error)
    )
  }
}
