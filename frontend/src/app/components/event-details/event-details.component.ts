import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventService} from "../../services/event.service";
import {Event} from "../../domain/event";

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {

  event: Event;
  constructor(private eventService : EventService,
              private route : ActivatedRoute,
              private router : Router) { }

  ngOnInit() {
    const id = Number(this.route.snapshot.paramMap.get('id'));

    if (isNaN(id)) {
      this.router.navigate(['/']);
      return;
    }

    this.eventService.getEventById(id).subscribe(
      data => this.event = data ,
      error => console.log(error)
    );
  }
}
