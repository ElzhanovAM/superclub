import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {Instructor} from "../../domain/instructor";
import {InstructorService} from "../../services/instructor.service";
import {BaseClub} from "../../domain/base-club";
import {ClubService} from "../../services/club.service";
import {AuthService} from "../../services/auth.service";
import {Account} from "../../domain/account";
import {Router} from "@angular/router";
declare const $ :any;

@Component({
  selector: 'app-instructor-add-modal',
  templateUrl: './instructor-add-modal.component.html',
  styleUrls: ['./instructor-add-modal.component.scss']
})
export class InstructorAddModalComponent implements OnInit {

  @Output() save = new EventEmitter();

  account : Account;
  instructor : Instructor;
  clubs : BaseClub[] = [];
  files: any;

  constructor(
    private clubService : ClubService,
    private instructorService : InstructorService,
    private authService: AuthService,
    private router: Router) {
    this.clear();
  }

  ngOnInit() {
    this.load();
    $(document).ready(function () {
      // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
      $('#instructorAddModal').modal({
        dismissible: false
      });
    });
  }

  onInsAdd() {
    this.clear();
    this.show();
  }

  onCancel() {
    this.close();
    this.clear();
  }

  onSave() {
    console.log(this.instructor);
    this.account = new Account(0, this.instructor.username, this.instructor.password);
    this.authService.addAccount(this.account).subscribe(
      data => {
        this.account.id = data.body;
        this.instructor.account = this.account;
        this.addInstructor()
      },
      error => console.log(error)
    );
  }

  addPhoto(event) {
    let target = event.target || event.srcElement;
    this.files = target.files;
    let files: FileList = this.files;
    const fd = new FormData();

    for (let i = 0; i < files.length; i++) {
      fd.append('instructorAvatar', files[i]);
    }

    this.instructorService.uploadFile(fd).subscribe(
      data => this.instructor.avatar = data.body ,
      error => console.log(error)
    );
  }

  private clear() {
    this.instructor = new Instructor(
      0,"", "", 0, null, [],null,[],0,"","", null, "", "");
  }

  private show() {
    $('#instructorAddModal').modal('open')
  }

  private close() {
    $('#instructorAddModal').modal('close')
  }

  private load() {
    this.clubService.getClubs().subscribe(
      data => this.clubs = data,
      error => console.log(error)
    )
  }

 /* private reset() {
    this.form.reset();
  }*/

  addRegalia(regalia:string) {
    this.instructor.regalia.push(regalia);
  }

  private addInstructor() {
    this.instructorService.addInstructor(this.instructor).subscribe(
      _ => {
        this.save.emit();
        this.close();
        this.clear();
      },
      error => {
        this.authService.deleteAccount(this.account.id).subscribe(
          _=>this.router.navigate(['/error']),
          error => console.log(error)
        );
      }
    )
  }
}
