import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorAddModalComponent } from './instructor-add-modal.component';

describe('InstructorAddModalComponent', () => {
  let component: InstructorAddModalComponent;
  let fixture: ComponentFixture<InstructorAddModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorAddModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
