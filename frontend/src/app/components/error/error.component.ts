import { Component, OnInit } from '@angular/core';
import {Error} from "../../domain/error";
import {ErrorService} from "../../services/error.service";

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  error: Error;

  constructor(private errorService : ErrorService) { }

  ngOnInit(
  ) {
    this.error = this.errorService.getError();
  }
}
