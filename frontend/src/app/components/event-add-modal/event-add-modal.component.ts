import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BaseClub} from "../../domain/base-club";
import {EventService} from "../../services/event.service";
import {Event} from "../../domain/event";
import {BaseInstructor} from "../../domain/base-instructor";
import {InstructorService} from "../../services/instructor.service";
import {ClubService} from "../../services/club.service";

declare const $ :any;

@Component({
  selector: 'app-event-add-modal',
  templateUrl: './event-add-modal.component.html',
  styleUrls: ['./event-add-modal.component.scss']
})
export class EventAddModalComponent implements OnInit {

  @Output() save = new EventEmitter();
  event : Event;
  clubs : BaseClub[] = [];
  instructors : BaseInstructor[] = [];
  files: any;

  constructor(
    private eventService : EventService,
    private clubService : ClubService,
    private instructorService : InstructorService){
    this.clear();
  }

  ngOnInit() {
    this.loadClubs();
    this.loadInstructors();
    $(document).ready(function(){
      // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
      $('#eventAddModal').modal(
        {
          dismissible : false
        }
      );
    });
  }

  onAdd() {
    this.clear();
    this.show();
  }

  onCancel() {
    this.close();
    this.clear()
  }

  onSave() {
    this.eventService.addEvent(this.event).subscribe(
      _ => {
        this.save.emit();
        this.close();
        this.clear();
      }
    )
  }

  addPhoto(event) {
    let target = event.target || event.srcElement;
    this.files = target.files;
    let files: FileList = this.files;
    const fd = new FormData();

    for (let i = 0; i < files.length; i++) {
      fd.append('eventPoster', files[i]);
    }

    this.eventService.uploadFile(fd).subscribe(
      data => this.event.poster = data.body ,
      error => console.log(error)
    );
  }

  private clear() {
    this.event = new Event(0,"", "", "", 0,0, null, "",null);
  }

  private show() {
    $('#eventAddModal').modal('open')
  }

  private close() {
    $('#eventAddModal').modal('close')
  }

  private loadClubs() {
    this.clubService.getClubs().subscribe(
      data => this.clubs = data,
      error => console.log(error)
    )
  }

  private loadInstructors() {
    this.instructorService.getInstructors().subscribe(
      data => this.instructors = data,
      error => console.log(error)
    )
  }
}
