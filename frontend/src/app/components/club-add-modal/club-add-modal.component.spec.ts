import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubAddModalComponent } from './club-add-modal.component';

describe('ClubAddModalComponent', () => {
  let component: ClubAddModalComponent;
  let fixture: ComponentFixture<ClubAddModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubAddModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubAddModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
