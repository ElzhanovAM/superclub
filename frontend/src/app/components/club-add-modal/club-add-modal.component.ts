import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ClubService} from "../../services/club.service";
import {BaseClub} from "../../domain/base-club";
import {ErrorService} from "../../services/error.service";
import {Router} from "@angular/router";
import {Error} from "../../domain/error";
declare const $ :any;

@Component({
  selector: 'app-club-add-modal',
  templateUrl: './club-add-modal.component.html',
  styleUrls: ['./club-add-modal.component.scss']
})
export class ClubAddModalComponent implements OnInit {
  @Output() save = new EventEmitter();
  club : BaseClub;
  files: any;
  error: Error;

  constructor(private clubService : ClubService,
              private errorService : ErrorService,
              private router: Router){
    this.clear();
  }

  ngOnInit() {
    $(document).ready(function(){
      // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
      $('#clubAddModal').modal(
        {
          dismissible : false
        }
      );
    });
  }

  onAdd() {
    this.clear();
    this.show();
  }

  onCancel() {
    this.close();
    this.clear()
  }

  onSave() {
    this.clubService.addClub(this.club).subscribe(
      _ => {
        this.save.emit();
        this.close();
        this.clear();
      },
      error => {
        this.error = new Error(error.code, error.message);
        this.errorService.setError(this.error);
        this.router.navigate(['/error'])
      }
    )
  }

  addPhoto(event) {
    let target = event.target || event.srcElement;
    this.files = target.files;
    let files: FileList = this.files;
    const fd = new FormData();

    for (let i = 0; i < files.length; i++) {
      fd.append('clubPoster', files[i]);
    }

    this.clubService.uploadFile(fd).subscribe(
      data => this.club.poster = data.body ,
      error => console.log(error)
    );
  }

  private clear() {
    this.club = new BaseClub(0,"", "", "", "");
  }

  private show() {
    $('#clubAddModal').modal('open')
  }

  private close() {
    $('#clubAddModal').modal('close')
  }
}
