import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ClubsComponent} from "./components/clubs/clubs.component";
import {ClubDetailsComponent} from "./components/club-details/club-details.component";
import {InstructorsComponent} from "./components/instructors/instructors.component";
import {EventsComponent} from "./components/events/events.component";
import {InstructorComponent} from "./components/instructor/instructor.component";
import {EventDetailsComponent} from "./components/event-details/event-details.component";
import {LoginComponent} from "./components/login/login.component";
import {ClubContainerComponent} from "./components/club-container/club-container.component";
import {InstructorContainerComponent} from "./components/instructor-container/instructor-container.component";
import {EventContainerComponent} from "./components/event-container/event-container.component";
import {MainPageComponent} from "./components/main-page/main-page.component";
import {ErrorComponent} from "./components/error/error.component";

const routes: Routes = [
  {path: 'clubs', component: ClubContainerComponent, children: [
      {path: '', component: ClubsComponent},
      {path: ':id', component: ClubDetailsComponent},
    ]},
  {path: 'instructors', component: InstructorContainerComponent, children: [
      {path: '', component: InstructorsComponent},
      {path: ':id', component: InstructorComponent},
    ]},
  {path: 'events', component: EventContainerComponent, children: [
      {path: '', component: EventsComponent},
      {path: ':id', component: EventDetailsComponent},
    ]},
  {path: 'login', component: LoginComponent},
  {path: '', component: MainPageComponent},
  {path: 'error', component: ErrorComponent},
  {path:'**', redirectTo:''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
