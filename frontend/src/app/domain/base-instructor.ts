export class BaseInstructor {
  constructor (
    public id : number,
    public fullName : string,
    public avatar : string,
    public phone : string
  ){}
}
