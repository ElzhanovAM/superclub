export class BaseEvent {
  constructor (
    public id : number,
    public title : string,
    public date : string,
    public poster : string,
    public go : number,
    public mayBe : number
  ){}
}
