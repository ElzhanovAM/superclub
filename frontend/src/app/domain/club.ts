import {BaseClub} from "./base-club";
import {BaseInstructor} from "./base-instructor";
import {BaseEvent} from "./base-event";

export class Club extends BaseClub{
  constructor (
    id : number,
    name : string,
    city : string,
    street : string,
    poster : string,
    public events : BaseEvent[],
    public instructors : BaseInstructor[]){
    super(id, name, city, street, poster);
  }


}
