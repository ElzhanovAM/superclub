import {BaseEvent} from "./base-event";
import {BaseClub} from "./base-club";
import {BaseInstructor} from "./base-instructor";

export class Event extends BaseEvent{
  constructor (
    id : number,
    title : string,
    date : string,
    poster : string,
    go : number,
    mayBe : number,
    public organizer : BaseInstructor,
    public description : string,
    public club : BaseClub){
    super(id, title, date, poster, go, mayBe);
  }
}
