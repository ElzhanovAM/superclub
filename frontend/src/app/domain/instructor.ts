import {BaseInstructor} from "./base-instructor";
import {BaseEvent} from "./base-event";
import {BaseClub} from "./base-club";
import {Account} from "./account";

export class Instructor extends BaseInstructor{
  constructor (
    id : number,
    fullName : string,
    avatar : string,
    public age : number,
    public startTraining : Date,
    public regalia  : string[],
    public club : BaseClub,
    public events : BaseEvent[],
    public experience : number,
    public vkProfile : string,
    public phone : string,
    public account: Account,
    public username: string,
    public password: string){
    super(id, fullName, avatar, phone);
  }
}
