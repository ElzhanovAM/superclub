export class BaseClub {
  constructor (
    public id : number,
    public name : string,
    public city : string,
    public street : string,
    public poster: string
  ){}
}
