export class EventSearch {
  constructor (
    public instructor : string,
    public city : string,
    public dateFrom : string,
    public dateTo : string
  ){}
}
