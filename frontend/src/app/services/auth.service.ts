import { Injectable } from '@angular/core';
import {Authentication} from "../domain/authetication";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs/Observable";
import {Account} from "../domain/account";

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) {
  }

  authenticate(auth: Authentication) {
    this.http.post(
      `${environment.auth}/access`,
      auth).subscribe((data: any) => {
      this.setAuth(data.token);
      this.router.navigate(['/clubs']);
    });
  }

  getAuth(): string {
    return localStorage.getItem('token');
  }

  setAuth(token: string) {
    localStorage.setItem('token', token);
  }

  removeAuth() {
    localStorage.removeItem('token');
  }

  isAuth() {
    return localStorage.getItem('token') !== null;
  }

  addAccount(account: Account) : Observable<any> {
    return this.http.post(
      `${environment.api}/account`,
      account,{
        responseType : 'text',
        observe : 'response'
      });
  }

  deleteAccount(id: number) : Observable<any> {
    return this.http.delete(`${environment.api}/account/${id}`,
      {
        responseType : 'text',
        observe : 'response'
      })
  }
}
