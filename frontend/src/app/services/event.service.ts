import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BaseEvent} from "../domain/base-event";
import {Event} from "../domain/event";
import {EventSearch} from "../domain/event-search";

@Injectable()
export class EventService {

  constructor(private http: HttpClient) { }

  getEvents(search : EventSearch) : Observable<BaseEvent[]> {
    return this.http.post<BaseEvent[]>(`${environment.api}/events`,
      search
      );
  }

  getEventById(id: number) : Observable<Event> {
    return this.http.get<Event>(`${environment.api}/events/${id}`);
  }

  addEvent(event: Event) : Observable<any> {
    return this.http.post(`${environment.api}/events/add`,
      event, {
        responseType : 'text',
        observe : 'response'
      })
  }

  deleteEvent(id: number) : Observable<any> {
    return this.http.delete(`${environment.api}/events/${id}`,
      {
        responseType : 'text',
        observe : 'response'
      })
  }

  go(id: number): Observable<Object> {
    return this.http.put(
      `${environment.api}/events/${id}/go`,
      null,
      {
        responseType: 'text',
        observe: 'response'
      }
    );
  }

  mayBe(id: number): Observable<Object> {
    return this.http.put(
      `${environment.api}/events/${id}/maybe`,
      null,
      {
        responseType: 'text',
        observe: 'response'
      }
    );
  }

  uploadFile(fd: FormData) : Observable<any>{
    return this.http.post(`${environment.api}/events/poster`,
      fd, {
        responseType : 'text',
        observe : 'response'
      })
  }
}
