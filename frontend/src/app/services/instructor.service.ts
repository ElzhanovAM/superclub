import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BaseInstructor} from "../domain/base-instructor";
import {Instructor} from "../domain/instructor";

@Injectable()
export class InstructorService {

  constructor(private http: HttpClient) { }

  getInstructors() : Observable<BaseInstructor[]> {
    return this.http.get<BaseInstructor[]>(`${environment.api}/instructors`);
  }

  getInstructorById(id: number) : Observable<Instructor> {
    return this.http.get<Instructor>(`${environment.api}/instructors/${id}`);
  }

  addInstructor(instructor: Instructor) : Observable<any> {
    return this.http.post(`${environment.api}/instructors`,
      instructor, {
        responseType : 'text',
        observe : 'response'
      })
  }

  deleteInstructor(id: number) : Observable<any> {
    return this.http.delete(`${environment.api}/instructors/${id}`,
      {
        responseType : 'text',
        observe : 'response'
      })
  }

  uploadFile(fd: FormData) : Observable<any>{
    return this.http.post(`${environment.api}/instructors/poster`,
      fd, {
        responseType : 'text',
        observe : 'response'
      })
  }
}
