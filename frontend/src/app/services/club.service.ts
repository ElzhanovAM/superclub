import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {BaseClub} from "../domain/base-club";
import {environment} from "../../environments/environment";
import {Club} from "../domain/club";

@Injectable()
export class ClubService {

  constructor(private http: HttpClient) { }

  getClubs() : Observable<BaseClub[]> {
    return this.http.get<BaseClub[]>(`${environment.api}/clubs`);
  }

  getClubById(id: number) : Observable<Club> {
    return this.http.get<Club>(`${environment.api}/clubs/${id}`);
  }

  addClub(club: BaseClub) : Observable<any> {
    return this.http.post(`${environment.api}/clubs`,
      club, {
        responseType : 'text',
        observe : 'response'
      })
  }

  deleteClub(id: number) : Observable<any> {
    return this.http.delete(`${environment.api}/clubs/${id}`,
       {
        responseType : 'text',
        observe : 'response'
      })
  }

  uploadFile(fd: FormData) : Observable<any>{
    return this.http.post(`${environment.api}/clubs/poster`,
      fd, {
        responseType : 'text',
        observe : 'response'
      })
  }
}
