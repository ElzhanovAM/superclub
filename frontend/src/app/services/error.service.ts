import { Injectable } from '@angular/core';
import {Error} from "../domain/error";

@Injectable()
export class ErrorService {

  error : Error;

  constructor() { }

  setError(error: any) {
    this.error = error;
  }

  getError(): Error {
    return this.error;
  }
}
