import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ClubsComponent } from './components/clubs/clubs.component';
import { ClubDetailsComponent } from './components/club-details/club-details.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ClubService} from "./services/club.service";
import { ClubComponent } from './components/club/club.component';
import { ClubAddModalComponent } from './components/club-add-modal/club-add-modal.component';
import {FormsModule} from "@angular/forms";
import { InstructorComponent } from './components/instructor/instructor.component';
import { EventComponent } from './components/event/event.component';
import { InstructorsComponent } from './components/instructors/instructors.component';
import { EventsComponent } from './components/events/events.component';
import {InstructorService} from "./services/instructor.service";
import {EventService} from "./services/event.service";
import { InstructorAddModalComponent } from './components/instructor-add-modal/instructor-add-modal.component';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { EventAddModalComponent } from './components/event-add-modal/event-add-modal.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import {AuthInterceptor} from "./interceptors/auth-interceptor";
import { ClubContainerComponent } from './components/club-container/club-container.component';
import { InstructorContainerComponent } from './components/instructor-container/instructor-container.component';
import { EventContainerComponent } from './components/event-container/event-container.component';
import { LoginEnterComponent } from './components/login-enter/login-enter.component';
import {AuthService} from "./services/auth.service";
import { MainPageComponent } from './components/main-page/main-page.component';
import { ErrorComponent } from './components/error/error.component';
import {ErrorService} from "./services/error.service";

@NgModule({
  declarations: [
    AppComponent,
    ClubsComponent,
    ClubDetailsComponent,
    ClubComponent,
    ClubAddModalComponent,
    InstructorComponent,
    EventComponent,
    InstructorsComponent,
    EventsComponent,
    InstructorAddModalComponent,
    EventDetailsComponent,
    EventAddModalComponent,
    LoginComponent,
    LogoutComponent,
    ClubContainerComponent,
    InstructorContainerComponent,
    EventContainerComponent,
    LoginEnterComponent,
    MainPageComponent,
    ErrorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
    ],
  providers: [
    ClubService,
    InstructorService,
    EventService,
    AuthService,
    ErrorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
